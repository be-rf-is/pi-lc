#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    PI-LC is a small GUI used to monitor voltages and digital input.
    It has been developped in order to be integrated in the test stand of
    the 10 MHz Feedback amplifiers' of the PS RF system at CERN.

    Usage :

        >>> from pilc import PILC
        >>> runPiLC()
"""

__version__ = "2.0.1"

from pilc import *
