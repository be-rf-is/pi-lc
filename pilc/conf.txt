<CONF>
<MISC>
	DAQ : 0;
	FLASH : 4;
	FLASHTRIGGER : 50.0;
	FLASHPOWERCOMMAND : 0;
	FLASHFAN : 1;
</MISC>
<MEASURE LABEL="UA_Driver">
        	PORT : 5;
        	ROW : 1;
        	COL : 2;
        	RATIO : 292.9;
        	MIN : 750.0;
        	MAX : 850.0;
        	TICK : 100;
</MEASURE>
<MEASURE LABEL="UA_Pre_Driver">
        	PORT : 4;
        	ROW : 0;
        	COL : 2;
        	RATIO : 292.9;
        	MIN : 550.0;
        	MAX : 650.0;
        	TICK : 100;
</MEASURE>
<MEASURE LABEL="UG1_Driver">
        	PORT : 1;
        	ROW : 1;
        	COL : 0;
        	RATIO : -17.08;
        	MIN : -15.0;
        	MAX : -10.0;
        	TICK : 10;
</MEASURE>
<MEASURE LABEL="UG1_Pre_Driver">
        	PORT : 0;
        	ROW : 0;
        	COL : 0;
        	RATIO : -17.08;
        	MIN : -40.0;
        	MAX : -15.0;
        	TICK : 10;
</MEASURE>
<MEASURE LABEL="UG2_Driver">
        	PORT : 3;
        	ROW : 1;
        	COL : 1;
        	RATIO : 146.48;
        	MIN : 180.0;
        	MAX : 220.0;
        	TICK : 100;
</MEASURE>
<MEASURE LABEL="UG2_Pre_Driver">
        	PORT : 2;
        	ROW : 0;
        	COL : 1;
        	RATIO : 146.48;
        	MIN : 180.0;
        	MAX : 220.0;
        	TICK : 100;
</MEASURE>
<FAN>
        	START : 3;
        	READ : 0;
        	WRITE : 0;
        	RISETIME : 60;
        	REDLED : 2;
        	GREENLED : 1;
        	RELAY : 0;
        	SWITCH : 1;
</FAN>
</CONF>
