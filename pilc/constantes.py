import os
from pathlib import Path

DIR_PATH = os.path.dirname(os.path.realpath(__file__))
CONF_FILE = os.path.join(DIR_PATH,"conf.txt")
NEEDLE_IMAGE = os.path.join(DIR_PATH,"data/icons/needle.png")
FAN_IMAGE = os.path.join(DIR_PATH,"data/icons/fan.zip")
KV_FILE = os.path.join(DIR_PATH,"frame.kv")
DAC_IMAGE = os.path.join(DIR_PATH,"data/icons/DAQC.jpg")
CERN_LOGO = os.path.join(DIR_PATH,"data/logo_cern.png")
BG_FILE = os.path.join(DIR_PATH,"data/background.png")
MEASURES_ICON = os.path.join(DIR_PATH,"data/icons/measures.png")
COGS_ICON = os.path.join(DIR_PATH,"data/icons/cogs.png")
WIRING_ICON = os.path.join(DIR_PATH,"data/icons/wiring.png")

HOME_PATH = str(Path.home())

#1 Create the path /home/user/.pilc/conf.txt
HOME_CONF_DIR = os.path.join(HOME_PATH, ".pilc")
HOME_CONF_FILE = os.path.join(HOME_PATH, ".pilc", "conf.txt")
#2 Check if path exist
if os.path.exists(HOME_CONF_DIR) :
    DIR = True
    if not os.path.exists(HOME_CONF_FILE) :
        #Create the FILE
        FILE = False
    else :
        FILE = True
else :
    DIR = False
    FILE = False

if not DIR :
    os.makedirs(HOME_CONF_DIR)
if not FILE :
    conf_file = open(CONF_FILE,"r")
    conf_file_content = conf_file.read()
    conf_file.close()
    home_conf_file = open(HOME_CONF_FILE, "w")
    home_conf_file.write(conf_file_content)
    home_conf_file.close()
