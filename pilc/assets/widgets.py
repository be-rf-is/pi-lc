#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from kivy_garden.speedmeter import SpeedMeter
from kivy.uix.boxlayout import BoxLayout
from kivy.core.text import Label as CoreLabel
from kivy.uix.label import Label
from kivy.graphics import Rectangle, Color
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.uix.progressbar import ProgressBar
from kivy.properties import ListProperty
from kivy.uix.textinput import TextInput
from kivy.uix.slider import Slider
from kivy.uix.switch import Switch
from kivy.uix.togglebutton import ToggleButton
from pilc.constantes import *
import math
import time
try :
    import piplates.DAQCplate as DAQ
except :
    print(""" The was an error while importing piplates module.
    Is the SPI interface enabled ? (use raspi-config to check)""")
    exit()

__all__ = ['PortButton', 'FanForm', 'MeasureForm', 'VuMeter', 'FanMeter', 'FlashLight', 'FlashForm']

class PortButton(BoxLayout):
    """Class used to create the button array displayed in the pinout screen.
    This class allows the software to check all the current used buttons and
    prevent double assignations"""
    def __init__(self, nb_button, port, group, selected, **kwargs):
        super(PortButton, self).__init__(**kwargs)
        self.orientation = "horizontal"
        self.size_hint = (1, 1)
        self.dict_button = {}

        for i in range(nb_button):
            if i == selected :
                self.dict_button[i] = ToggleButton(text=port + str(i), group=group, state ="down")
                self.selected = self.dict_button[i]
            else :
                self.dict_button[i] = ToggleButton(text=port + str(i), group=group)
            self.add_widget(self.dict_button[i])

    def update_group(self, group):
        """ Method used when the label is changed in the parameters screens.
        The buttons are grouped by the label. So if the label change, we've to
        change the group value"""
        for key in self.dict_button.keys():
            self.dict_button[key].group = group

    def return_used(self):
        """Return the current used button name -> Means the current button"""
        for key in self.dict_button.keys():
            if self.dict_button[key].state == "down" :
                return self.dict_button[key].text

    def return_number(self):
        """Return the current used button number -> Means the current port"""
        for key in self.dict_button.keys():
            if self.dict_button[key].state == "down" :
                return int(self.dict_button[key].text[-1])

    def set_used(self, number):
        """ Set the "number" button to used, all others to unused. This method
        is used to reset the parameters"""
        for key in self.dict_button.keys():
            if int(self.dict_button[key].text[-1]) == number :
                self.dict_button[key].state = "down"
            else :
                self.dict_button[key].state = "normal"

class FlashForm(BoxLayout):
    """ Class used to create the form used to change the flashlight parameters"""
    def __init__(self, flash_conf, **kwargs):
        super(FlashForm, self).__init__(**kwargs)
        self.orientation = "vertical"
        self.size_hint = (1, 1)
        self.padding = 10
        self.spacing = 10
        #Trigger Input Form
        self.trigger_layout = BoxLayout(orientation = "horizontal")
        self.trigger_layout.add_widget(Label(text="Flash Light Voltage Trigger", size_hint = (0.5,1)))
        self.trigger_form = Slider(min=0, max=1000, step=10, value=flash_conf.trigger,
        size_hint = (0.4, 1) )
        self.trigger_text = Label(text = str(int(self.trigger_form.value)) +"V", size_hint = (0.1,1))
        self.trigger_layout.add_widget(self.trigger_form)
        self.trigger_layout.add_widget(self.trigger_text)
        self.trigger_form.bind(value = self.onTriggerChange)
        self.add_widget(self.trigger_layout)

        #Button Flash Fan
        self.fan_layout = BoxLayout(orientation = "horizontal")
        self.fan_layout.add_widget(Label(text="Trigger Fan Status", size_hint = (0.5,1)))
        self.fan_form = Switch(active = flash_conf.flash_fan, size_hint=(0.5, 1))
        self.fan_layout.add_widget(self.fan_form)
        self.add_widget(self.fan_layout)

        #Button Flash Power
        self.power_layout = BoxLayout(orientation = "horizontal")
        self.power_layout.add_widget(Label(text="Trigger Power Command", size_hint = (0.5,1)))
        self.power_form = Switch(active = flash_conf.flash_power, size_hint=(0.5, 1))
        self.power_layout.add_widget(self.power_form)
        self.add_widget(self.power_layout)



    def onTriggerChange(self, instance, value):
        self.trigger_text.text = str(int(value))+"V"

    def reload_tab(self, old_conf):
        self.trigger_form.value = old_conf.trigger
        self.trigger_text.text = str(int(old_conf.trigger))+"V"
        self.power_form.active = old_conf.flash_power
        self.fan_form.active = old_conf.flash_fan

class FanForm(BoxLayout):
    """ Class used to create the form used to change the rise time"""
    def __init__(self, fan_conf, **kwargs):
        super(FanForm, self).__init__(**kwargs)
        self.orientation = "vertical"
        self.size_hint = (1, 1)
        self.padding = 10
        self.spacing = 10
        #Rise Time Input Form
        self.rise_layout = BoxLayout(orientation = "horizontal")
        self.rise_layout.add_widget(Label(text="Rise Time", size_hint = (0.5,1)))
        self.rise_form = Slider(min=1, max=180, step=1, value=fan_conf.risetime,
        size_hint = (0.4, 1) )
        self.rise_text = Label(text = str(int(self.rise_form.value)) +"s", size_hint = (0.1,1))
        self.rise_layout.add_widget(self.rise_form)
        self.rise_layout.add_widget(self.rise_text)
        self.rise_form.bind(value = self.onRiseChange)
        self.add_widget(self.rise_layout)

    def onRiseChange(self, instance, value):
        self.rise_text.text = str(int(value))+"s"

    def reload_tab(self, old_conf):
        self.rise_form.value = old_conf.risetime
        self.rise_text.text = str(int(old_conf.risetime))+"s"

class MeasureForm(BoxLayout):
    """ MeasureForm correspond to the form allowing the user to modify the
    parameters of a single measure. All the "on......" method are binded"""
    def __init__(self, measure_conf, **kwargs):
        super(MeasureForm, self).__init__(**kwargs)
        self.orientation = "vertical"
        self.size_hint = (1, 1)
        self.padding = 10
        self.spacing = 10

        #Label Input Form
        self.label_layout = BoxLayout(orientation = "horizontal")
        self.label_layout.add_widget(Label(text="Label", size_hint = (0.5,1)))
        self.label_form = TextInput(text=measure_conf.label, multiline=False, size_hint = (0.5,1))
        self.label_layout.add_widget(self.label_form)
        self.add_widget(self.label_layout)

        #Row Input Form
        self.row_layout = BoxLayout(orientation = "horizontal")
        self.row_layout.add_widget(Label(text="Row", size_hint = (0.5,1)))
        self.row_form = Slider(min=0, max=6, step=1, value=measure_conf.line_number,
        size_hint = (0.4, 1) )
        self.row_text = Label(text = str(int(self.row_form.value)), size_hint = (0.1,1))
        self.row_layout.add_widget(self.row_form)
        self.row_layout.add_widget(self.row_text)
        self.row_form.bind(value = self.onRowChange)
        self.add_widget(self.row_layout)

        #Col Input Form
        self.col_layout = BoxLayout(orientation = "horizontal")
        self.col_layout.add_widget(Label(text="Col", size_hint = (0.5,1)))
        self.col_form = Slider(min=0, max=6, step=1, value=measure_conf.col_number,
        size_hint = (0.4, 1) )
        self.col_text = Label(text = str(int(self.col_form.value)), size_hint = (0.1,1))
        self.col_layout.add_widget(self.col_form)
        self.col_layout.add_widget(self.col_text)
        self.col_form.bind(value = self.onColChange)
        self.add_widget(self.col_layout)

        #Ratio Input Form
        self.ratio_layout = BoxLayout(orientation = "horizontal")
        self.ratio_layout.add_widget(Label(text="Ratio", size_hint = (0.5,1)))
        self.ratio_form = TextInput(text=str(measure_conf.ratio), multiline=False,
        size_hint = (0.5,1), input_filter = 'float')
        self.ratio_form.bind(text = self.onRatioChange)
        self.ratio_layout.add_widget(self.ratio_form)
        self.add_widget(self.ratio_layout)

        #Min Input Form
        self.min_layout = BoxLayout(orientation = "horizontal")
        self.min_layout.add_widget(Label(text="Min Value", size_hint = (0.5,1)))
        self.min_form = Slider(min=min(0, round(4.096*float(self.ratio_form.text))),
        max=measure_conf.max, step=1,
        value=measure_conf.min_acceptable,
        size_hint = (0.4, 1) )
        self.min_text = Label(text = str(float(self.min_form.value)), size_hint = (0.1,1))
        self.min_layout.add_widget(self.min_form)
        self.min_layout.add_widget(self.min_text)
        self.min_form.bind(value = self.onMinChange)
        self.add_widget(self.min_layout)

        #Max Input Form
        self.max_layout = BoxLayout(orientation = "horizontal")
        self.max_layout.add_widget(Label(text="Max Value", size_hint = (0.5,1)))
        self.max_form = Slider(min=measure_conf.min,
        max=max(0, round(4.096*float(self.ratio_form.text))), step=1,
        value=measure_conf.max_acceptable,
        size_hint = (0.4, 1) )
        self.max_text = Label(text = str(float(self.max_form.value)), size_hint = (0.1,1))
        self.max_layout.add_widget(self.max_form)
        self.max_layout.add_widget(self.max_text)
        self.max_form.bind(value = self.onMaxChange)
        self.add_widget(self.max_layout)

        #Tick Input Form
        self.tick_layout = BoxLayout(orientation = "horizontal")
        self.tick_layout.add_widget(Label(text="Tick Value", size_hint = (0.5,1)))
        self.tick_form = Slider(min=0,max=3, step=1, value=round(math.log(measure_conf.tick,10)),
        size_hint = (0.4, 1) )
        self.tick_text = Label(text = str(10**round(math.log(measure_conf.tick,10))), size_hint = (0.1,1))
        self.tick_layout.add_widget(self.tick_form)
        self.tick_layout.add_widget(self.tick_text)
        self.tick_form.bind(value = self.onTickChange)
        self.add_widget(self.tick_layout)

    def onRowChange(self, instance, value):
        self.row_text.text = str(int(value))

    def onColChange(self, instance, value):
        self.col_text.text = str(int(value))

    def onRatioChange(self, instance, value):
        #On ratio change, we've to update the min and max value
        try :
            mini = round(min(0, 4.096*float(value)))
            maxi = round(max(0, 4.096*float(value)))
            self.min_form.min = mini
            self.max_form.max = maxi
        except :
            pass

    def onMinChange(self, instance, value):
        self.min_text.text = str(float(value))
        self.max_form.min = value

    def onMaxChange(self, instance, value):
        self.max_text.text = str(float(value))
        self.min_form.max = value

    def onTickChange(self, instance, value):
        self.tick_text.text = str(10**int(value))

    def confToArray(self):
        return [
            self.label_form.text,
            int(self.row_text.text),
            int(self.col_text.text),
            self.ratio_form.text,
            self.min_form.value,
            self.max_form.value,
            int(self.tick_text.text),
        ]

    def reload_tab(self, old_conf):
        self.label_form.text = old_conf.label
        self.row_form.value = old_conf.line_number
        self.row_text.text = str(int(old_conf.line_number))
        self.col_form.value = old_conf.col_number
        self.col_text.text = str(int(old_conf.col_number))
        self.ratio_form.text = str(old_conf.ratio)
        self.min_form.value = old_conf.min_acceptable
        self.min_text.text = str(float(self.min_form.value))
        self.max_form.value = old_conf.max_acceptable
        self.max_text.text = str(float(self.max_form.value))
        self.tick_form.value = round(math.log(old_conf.tick,10))
        self.tick_text.text = str(10**round(math.log(old_conf.tick,10)))

class FlashLight:
    def __init__(self, dac_address, port, trigger, flash_power, flash_fan):
        self.dac_address = dac_address
        self.port = port
        self.trigger = trigger
        self.flash_power = False if flash_power == 0 else True
        self.flash_fan = False if flash_fan == 0 else True
    def toggleFlash(self, fan_state, liste_val):
        flash_fan = [True if (self.flash_fan and fan_state[0]) else False]
        flash_power = [True if (self.flash_power and fan_state[1]) else False]
        flash_measures = [val > self.trigger for val in liste_val]
        final = flash_measures + flash_power + flash_fan
        if any(final):
            #TurnOn FlashLight
            DAQ.setDOUTbit(self.dac_address, self.port)
        else :
            #Turn Off Flash Light
            DAQ.clrDOUTbit(self.dac_address, self.port)

class VuMeter(SpeedMeter):
    """ The VuMeter Class correspond to a single VuMeter. It's inherit from the
    SpeedMeter class comming from kivy_garden. This inheritage is helpfull in
    order to add a text label corresponding to the value"""
    def __init__(self, dict_conf, **kwargs):
        super(VuMeter, self).__init__(**kwargs)
        ###Parameters Commin from Conf
        self.dac_address = int(dict_conf['DAC_ADDRESS'])
        self.port = int(dict_conf['PORT'])
        self.line_number = int(dict_conf['ROW'])
        self.col_number = int(dict_conf['COL'])
        self.ratio = dict_conf['RATIO']
        self.min_acceptable = dict_conf['MIN']
        self.max_acceptable = dict_conf['MAX']

        ###Prameters form inheritage
        self.tick = int(dict_conf["TICK"])
        self.label = dict_conf['LABEL']
        self.min = int(min(0, round(self.ratio * 4.096, -1)))
        self.max = int(max(0, round(self.ratio * 4.096, -1)))
        self.sectors =  [self.min, '#d9534f',
                         int(self.min_acceptable), '#5cb85c',
                         int(self.max_acceptable), '#d9534f', self.max]

        #Paremeters not going to change
        self.start_angle = -120
        self.end_angle = 120
        self.value_font_size = 12
        self.subtick = 2
        self.needle_image = NEEDLE_IMAGE
        self.shadow_color = "#5bc0de"

        #Setting default value and adding value Label
        self.value = self.min
        self.label_measure = CoreLabel(text=str(self.value) + " V", markup=True, bold=True,
                          font_size=20, color = [0.36, 0.72, 0.36, 1])
        self.label_measure.refresh()
        self.label_texture = self.label_measure.texture
        self.tw, self.th = self.label_texture.size
        with self.canvas :
            self.label_2 = Rectangle(pos=self.center,
                                     size=(self.tw, self.th),
                                     texture=self.label_texture,
                                     color=Color([1,1,1,1]))
        self.bind(pos=self.update_label_2, size=self.update_label_2)

    def update_label_2(self, *args):
        """Binded method used to position and resize the value label"""
        self.label_2.pos = self.center_x - self.tw/2, self.center_y - self.height/2 + self.th
        self.label_2.size = (self.tw, self.th)

    def update_val(self):
        """ Method used to refresh the VuMeter. It reads the new value and update
        both the needle position, the value label and its color"""
        self.value = round(DAQ.getADC(self.dac_address, self.port) * float(self.ratio),2)
        if self.sectors[2] <= self.value <= self.sectors[4]:
            color = [0.36, 0.72, 0.36, 1]
        else :
            color = [0.85, 0.33, 0.31, 1]
        self.label_measure = CoreLabel(text="{} V".format(str(self.value)), markup=True, bold=True,
                  font_size=20, color = color)

        self.label_measure.refresh()
        self.label_texture = self.label_measure.texture
        self.tw, self.th = self.label_texture.size
        self.label_2.size = self.tw, self.th
        self.label_2.pos = self.center_x - self.tw/2, self.center_y - self.height/2 + self.th
        self.label_2.texture = self.label_texture
        return abs(self.value)

class FanMeter(BoxLayout):
    """ The FanMeter Class correspond to a row displaying all the fan and power
    control.
        -> Fan status : text and propeler animation
        -> Power stauts : with a ProgressBar
        -> Force and inhib head control : with button

    It also implement the refresh Method
    """
    def __init__(self, dict_conf, **kwargs):
        super(FanMeter, self).__init__(**kwargs)
        ###Paremeters used to communicate
        self.start = int(dict_conf['START'])
        self.dac_address = int(dict_conf['DAC_ADDRESS'])
        self.read = int(dict_conf['READ'])
        self.write = int(dict_conf['WRITE'])
        self.risetime = int(dict_conf['RISETIME'])
        self.redled = int(dict_conf['REDLED'])
        self.greenled = int(dict_conf['GREENLED'])
        self.relay = int(dict_conf['RELAY'])
        self.switch = int(dict_conf['SWITCH'])

        ###Paremeters used for display
        self.orientation = "horizontal"
        self.size_hint = (1, 0.20)
        self.padding = 5
        self.fan_status = False

        #heat parameters :
        self.power_status = False
        self.power_command = 0.0
        self.power_percent = 0.0
        self.power_time = None

        #Parameters used to blink the green Led
        self.startLed = time.time()
        self.valLed = False

        #Fan Control :
        self.fan_control_layout = BoxLayout(orientation = "vertical", size_hint=(0.1, 1))
        self.fan_control_label = Label(text= "Fan Control", size_hint=(1, 0.5))
        self.fan_control_switch = Switch(active = False, size_hint=(1, 0.5))
        self.fan_control_layout.add_widget(self.fan_control_label)
        self.fan_control_layout.add_widget(self.fan_control_switch)


        #Propeler Image
        self.propeler_image = Image(source=FAN_IMAGE,
        size_hint=(0.2, 1), anim_delay= -1, allow_stretch =True, mipmap= True,)

        #Creating the three text widget
        self.text_layout = BoxLayout(orientation = "vertical", size_hint=(0.4, 1), spacing = 5)
        self.fan_status_text = Label(text="FAN OK", font_size='20sp', size_hint=(1,0.5))
        self.heat_status_text = Label(text="Heating in progress", font_size='20sp', size_hint=(1,0.25))
        self.progressbar = ProgressBar(max = 100, value = 0, size_hint=(1,0.25))
        self.text_layout.add_widget(self.fan_status_text)
        self.text_layout.add_widget(self.heat_status_text)
        self.text_layout.add_widget(self.progressbar)

        #Creating the two button line
        self.force_heat_layout = BoxLayout(orientation = "horizontal")
        self.force_heat_label = Label(text= "Force Heat", size_hint=(0.5,1))
        self.force_heat_switch = Switch(active = False, size_hint=(0.5, 1))
        self.force_heat_layout.add_widget(self.force_heat_label)
        self.force_heat_layout.add_widget(self.force_heat_switch)

        self.inhib_heat_layout = BoxLayout(orientation = "horizontal")
        self.inhib_heat_label = Label(text= "Inhib Heat", size_hint=(0.5,1))
        self.inhib_heat_switch = Switch(active = False, disabled = True, size_hint=(0.5, 1))
        self.inhib_heat_layout.add_widget(self.inhib_heat_label)
        self.inhib_heat_layout.add_widget(self.inhib_heat_switch)

        self.button_layout = BoxLayout(orientation = "vertical", size_hint=(0.3, 1))
        self.button_layout.add_widget(self.force_heat_layout)
        self.button_layout.add_widget(self.inhib_heat_layout)

        #Add the frame to the main one
        self.add_widget(self.fan_control_layout)
        self.add_widget(self.propeler_image)
        self.add_widget(self.text_layout)
        self.add_widget(self.button_layout)

    def toogleFan(self):
        """ Method used to update the FanMeter.
        According to the fan status and the control button (force heat, hard stop)
        it sets the power and control the led"""
        #Start and stop the fan
        if self.fan_control_switch.active :
            DAQ.setDOUTbit(self.dac_address, self.start)
            #Turn ON the FAN
        else :
            #Turn OFF the FAN
            DAQ.clrDOUTbit(self.dac_address, self.start)

        #Read Fan Status
        self.fan_status = DAQ.getDINbit(self.dac_address, self.read)

        #Check hard stop button
        hard_switch = (False if DAQ.getDINbit(self.dac_address, self.switch) == 1 else True)
        #Lock inhib heat and set it to the same value as hard switch
        self.inhib_heat_switch.active = hard_switch
        if hard_switch :
            #If hard stop, lock both force heat and inhib. Set force to 0 inhib to 1
            self.force_heat_switch.active = False
            self.force_heat_switch.disabled = True
        else :
            self.force_heat_switch.disabled = False
        #Setting Power Status
        self.power_status = ((self.fan_status and not self.inhib_heat_switch.active)
        or self.force_heat_switch.active) and not hard_switch

        #Toogle the power :
        if self.power_status :
            if self.power_time == None :
                self.power_time = time.time()
            self.power_command = min(2.5, (time.time() - self.power_time ) * (2.5/self.risetime))
            self.power_percent = round((self.power_command/2.5) * 100,2)
            DAQ.setDOUTbit(self.dac_address, self.relay)
        else :
            self.power_command = 0.0
            self.power_percent = 0.0
            self.power_time = None
            DAQ.clrDOUTbit(self.dac_address, self.relay)

        self.progressbar.value = self.power_percent
        #Setting the power :
        DAQ.setDAC(self.dac_address, self.write, self.power_command)

        #Setting the fan image color
        if self.fan_status :
            self.propeler_image.anim_delay = 0.05
            self.propeler_image.color = [0.36, 0.72, 0.36, 1]
            self.fan_status_text.text = "FAN OK"
            self.fan_status_text.color = [0.36, 0.72, 0.36, 1]
        else :
            self.propeler_image.anim_delay = -1
            self.propeler_image.color = [1.49, 0.07, -0.2, 1]
            self.fan_status_text.text = "NO FAN"
            self.fan_status_text.color = [0.85, 0.33, 0.31, 1]

        #Setting the heat status :
        if hard_switch :
            self.heat_status_text.text ="Hard stop enabled"
        elif self.force_heat_switch.active :
            if self.power_percent < 100 :
                self.heat_status_text.text = "Forced Heating in progress - {} %".format(self.power_percent)
            else :
                self.heat_status_text.text = "Warm (Forced)"
        elif self.inhib_heat_switch.active :
            self.heat_status_text.text = "Warm-up inhibited"
        elif self.power_status :
            if self.power_percent < 100 :
                self.heat_status_text.text = "Heating in progress - {} %".format(self.power_percent)
            else :
                self.heat_status_text.text = "Warm"
        else :
            self.heat_status_text.text ="No heat"

        #Setting the led :
        #Four cases :
            # 1) Solid Red : NO FAN
            # 2) Solid Orange : Hard Stop
            # 3) Solid Green : Warm
            # 4) Blink Green : Heating in progress

        if self.power_status :
            #First case : Warm allowed
            DAQ.clrDOUTbit(self.dac_address, self.redled)
            if self.power_command < 2.5 :
                # Blink
                now = time.time()
                if now - self.startLed > 0.2 :
                    self.startLed = now
                    self.valLed = not(self.valLed)
                    if self.valLed :
                        DAQ.setDOUTbit(self.dac_address, self.greenled)
                    else :
                        DAQ.clrDOUTbit(self.dac_address, self.greenled)
            else :
                # Green Led
                DAQ.setDOUTbit(self.dac_address, self.greenled)
        elif hard_switch :
            #Orange
            DAQ.setDOUTbit(self.dac_address, self.redled)
            DAQ.setDOUTbit(self.dac_address, self.greenled)

        else :
            # Red
            DAQ.setDOUTbit(self.dac_address, self.redled)
            DAQ.clrDOUTbit(self.dac_address, self.greenled)
        return(self.fan_control_switch.active, self.power_status)
