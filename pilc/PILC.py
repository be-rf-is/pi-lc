#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    PI-LC is a small GUI used to monitor voltages and digital input.
    It has been developped in order to be integrated in the test stand of
    the 10 MHz Feedback amplifiers' of the PS RF system at CERN.

    Usage :

        >>> from pilc import PILC
        >>> runPiLC()
"""

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.lang import Builder
from kivy.clock import mainthread
from kivy.uix.label import Label
from kivy.uix.slider import Slider
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.properties import NumericProperty,StringProperty, BooleanProperty, ListProperty
from kivy.uix.gridlayout import GridLayout
from kivy.uix.screenmanager import CardTransition
from kivy.uix.tabbedpanel import TabbedPanel, TabbedPanelItem
from kivy.uix.button import Button
from kivy.uix.popup import Popup
from kivy.uix.image import Image
from kivy.uix.togglebutton import ToggleButton
from kivy.core.window import Window
from pilc.assets.widgets import VuMeter, FanMeter, MeasureForm, FanForm, PortButton, FlashLight, FlashForm
from pilc.assets import sp
from pilc.constantes import *
import random
import math
import time
import signal

try :
    import piplates.DAQCplate as DAQ
except :
    print(""" The was an error while importing piplates module.
    Is the SPI interface enabled ? (use raspi-config to check)""")
    exit()

from kivy.config import Config

__all__ = ['runPiLC']

#A screen class correspond to a screen tab on the GUI
class MeasuresScreen(Screen):
    """ MeasuresScreen is the one used to display the values of the analog
    voltages as well as the status of the fan.
    """
    def __init__(self, measure_dict, fan, flash, **kwargs):
        super(Screen,self).__init__(**kwargs)
        self.name = "Measures"

        #Creating the grid for the measure widgets
        self.measure_layout = GridLayout(cols=3, padding = 10, size_hint=(1, 0.8))

        #Populating the grid
        self.dict_measures = measure_dict
        self.populateGrid()

        #Creating the fan Layout
        self.fan_layout = fan

        #Getting the flash
        self.flash = flash

        #Creating the main layout and add both the grid and the fan
        self.main_layout = BoxLayout(orientation='vertical', padding = 5)
        self.main_layout.add_widget(self.measure_layout)
        self.main_layout.add_widget(self.fan_layout)
        self.add_widget(self.main_layout)

    def update_pinout(self, dict_measure_pinout, DAC_ADDRESS):
        """ Method called when the configuration is changed in order to update
        the dictionary containing the configuration"""
        for key, value in self.dict_measures.items():
            self.dict_measures[key].port = dict_measure_pinout[key]
            self.dict_measures[key].dac_address = DAC_ADDRESS

    def update_measures(self, dict_param):
        """ Method called when the configuration is changed in order to update
        the parameters of the vu-meters.
        """
        key_to_remove = []
        for key, measureForm in dict_param.items() :
            new_conf = measureForm.confToArray()
            #Modify label :
            self.dict_measures[key].label = new_conf[0]

            #Modify Tick :
            self.dict_measures[key].tick = new_conf[6]
            #Modify min and max
            ratio = float(new_conf[3])
            self.dict_measures[key].ratio = new_conf[3]
            self.dict_measures[key].min = int(min(0, round(ratio * 4.096, -1)))
            self.dict_measures[key].max = int(max(0, round(ratio * 4.096, -1)))

            #Modify sectors
            self.dict_measures[key].sectors[0] =  self.dict_measures[key].min
            self.dict_measures[key].sectors[2] =  int(new_conf[4])
            self.dict_measures[key].sectors[4] =  int(new_conf[5])
            self.dict_measures[key].sectors[6] =  self.dict_measures[key].max
            self.dict_measures[key].min_acceptable = int(new_conf[4])
            self.dict_measures[key].max_acceptable = int(new_conf[5])

            #Update Line and COL
            self.dict_measures[key].line_number = new_conf[1]
            self.dict_measures[key].col_number = new_conf[2]

            #UpdateTheKeys :
            key_to_remove.append((key, new_conf[0]))

        #UpdateTheKeys :
        dict_bind_key = {}
        for index, key in enumerate(key_to_remove) :
            val = self.dict_measures.pop(key[0])
            self.dict_measures[key[1]] = val
            val2 = dict_param.pop(key[0])
            dict_param[key[1]] = val2
            dict_bind_key[key[0]] = key[1]

        self.populateGrid()

        return dict_param, dict_bind_key

    def populateGrid(self):
        """ The vu-meters are organized in a grid layout. The populateGrid Method
        is used to populate this grid according to the layout defined in the
        configuration file.
        """
        ###Sort Measure Configuration
        nb_rows = max(measure.line_number for name, measure in self.dict_measures.items()) + 1
        nb_cols = max(measure.col_number for name, measure in self.dict_measures.items()) + 1

        table_measure = []
        for nb_line in range(nb_rows):
            table_measure.append([False for nb_col in range(nb_cols)])

        for key, value in self.dict_measures.items():
            table_measure[value.line_number][value.col_number] = key

        #Updating position
        self.measure_layout.clear_widgets()
        for line_number, a_line in enumerate(table_measure):
            for col_number, an_item in enumerate(a_line):
                if an_item :
                    self.measure_layout.add_widget(self.dict_measures[an_item])
                else:
                    self.measure_layout.add_widget(Label())

    def saveConf(self):
        """ Method used to convert the dictionary to a conf.txt file.
        The method format the configuration in order to be reabable"""
        a_measure = """<MEASURE LABEL="{}">
        \tPORT : {};
        \tROW : {};
        \tCOL : {};
        \tRATIO : {};
        \tMIN : {};
        \tMAX : {};
        \tTICK : {};
</MEASURE>\n"""

        fan = """<FAN>
        \tSTART : {};
        \tREAD : {};
        \tWRITE : {};
        \tRISETIME : {};
        \tREDLED : {};
        \tGREENLED : {};
        \tRELAY : {};
        \tSWITCH : {};
</FAN>\n"""

        txt = """<CONF>
<MISC>
\tDAQ : {};
\tFLASH : {};
\tFLASHTRIGGER : {};
\tFLASHPOWERCOMMAND : {};
\tFLASHFAN : {};
</MISC>\n""".format(self.fan_layout.dac_address, int(self.flash.port), self.flash.trigger, (1 if self.flash.flash_power else 0), (1 if self.flash.flash_fan else 0))
        for key, val in self.dict_measures.items():
            txt += a_measure.format(val.label.replace(" ", "_"), val.port, val.line_number, val.col_number,
            val.ratio, val.min_acceptable, val.max_acceptable, val.tick)
        txt += fan.format(self.fan_layout.start, self.fan_layout.read, self.fan_layout.write, self.fan_layout.risetime,
        self.fan_layout.redled, self.fan_layout.greenled, self.fan_layout.relay,
        self.fan_layout.switch)

        txt += "</CONF>"

        conf_file = open(HOME_CONF_FILE,"w")
        conf_file.write(txt)
        conf_file.close()

class ParametersScreen(Screen):
    """ ParametersScreen is the one used to display parameters values and to
    update them.
    """
    def __init__(self, measure_dict, fan, flash, **kwargs):
        super(Screen,self).__init__(**kwargs)
        self.name = "Parameters"

        self.flash = flash

        self.main_layout = BoxLayout(orientation='vertical')
        self.tab_panel = TabbedPanel(do_default_tab=False, size_hint=(1,0.9))

        key_order = [key for key in measure_dict.keys()]
        key_order.sort()

        self.dict_tab = {}
        self.dict_Form = {}
        for key in key_order:
            self.dict_tab[key] = TabbedPanelItem(text=key)
            self.dict_Form[key] = MeasureForm(measure_dict[key])
            self.dict_tab[key].add_widget(self.dict_Form[key])
            self.tab_panel.add_widget(self.dict_tab[key])

        self.fan_tab = TabbedPanelItem(text="Transfo")
        self.fan_form = FanForm(fan)
        self.fan_tab.add_widget(self.fan_form)
        self.tab_panel.add_widget(self.fan_tab)

        self.flash_tab = TabbedPanelItem(text="FlashLight")
        self.flash_form = FlashForm(flash)
        self.flash_tab.add_widget(self.flash_form)
        self.tab_panel.add_widget(self.flash_tab)

        self.button_layout = BoxLayout(orientation='horizontal', size_hint=(1, 0.1))
        self.save_button = Button(text="Save", on_press=lambda a:self.save_pressed())
        self.button_layout.add_widget(self.save_button)
        self.reset_button = Button(text="Reset", on_press=lambda a:self.reload_pressed())
        self.button_layout.add_widget(self.reset_button)


        self.main_layout.add_widget(self.tab_panel)
        self.main_layout.add_widget(self.button_layout)
        self.add_widget(self.main_layout)

    def on_pre_enter(self):
        """ The on_pre_enter method is a binded method of the kivy environment.
        It's called when the screen is displayed, just before it appears.
        It's used to clean all the unsaved parameters"""
        self.reload_pressed()

    def reload_pressed(self):
        """ Reset the parameters to the last saved"""
        for key, form in self.dict_Form.items() :
            form.reload_tab(measuresScreen.dict_measures[key])
        self.fan_form.reload_tab(measuresScreen.fan_layout)
        self.flash_form.reload_tab(measuresScreen.flash)

    def save_pressed(self):
        """ Update all the objects to the new values"""
        liste_error = []
        liste_name = []
        liste_pos = []
        list_key_form = []
        for key, tab in self.dict_Form.items() :
            list_key_form.append(key)
            cur = tab.confToArray()
            if cur[0] not in liste_name :
                liste_name.append(cur[0])
            else :
                liste_error.append('Duplicated "{}" label'.format(cur[0]))
            try :
                float(cur[3])
            except :
                liste_error.append('"{}" is not a valid RATIO -{}-'.format(cur[3], cur[0]))
            if (cur[1], cur[2]) not in liste_pos :
                liste_pos.append((cur[1], cur[2]))
            else :
                liste_error.append('Duplicated "{}" position'.format((cur[1], cur[2])))

        if liste_error :
            txt = ""
            for err in liste_error :
                txt += err + "\n"
            Popup(title='There are some errors...',
            content=Label(text=txt),size_hint=(None, None), size=(400, 400)).open()
            return False

        else :
            #1 Modifiyng the widgets :
            self.dict_Form, key_bind = measuresScreen.update_measures(self.dict_Form)

            # Modifying the tab and the dict
            for key in list_key_form :
                val2 = self.dict_tab.pop(key)
                self.dict_tab[key_bind[key]] = val2
                self.dict_tab[key_bind[key]].text = key_bind[key]

            #Modifying the pinouts label name and dict
            pinoutScreen.update_key(key_bind)

            # Modifying Fan Time
            measuresScreen.fan_layout.risetime = int(self.fan_form.rise_form.value)

            # Modifying Flash Parameters
            measuresScreen.flash.trigger = int(self.flash_form.trigger_form.value)
            measuresScreen.flash.flash_power = self.flash_form.power_form.active
            measuresScreen.flash.flash_fan = self.flash_form.fan_form.active

            measuresScreen.saveConf()
            #Display confirmation popup
            Popup(title='Configuration Update',
            content=Label(text="The configuration has been saved"),
            size_hint=(None, None), size=(400, 400)).open()

class PinoutScreen(Screen):
    """PinoutScreen is used to display the current pinout of the DACQ plate.
    It also allows the user to dynamically change the pinout and save it.
    """
    def __init__(self,measure_dict, fan, flash, **kwargs):
        super(PinoutScreen,self).__init__(**kwargs)
        self.name = "Pinout"
        self.main_layout = BoxLayout(orientation='vertical')
        self.tab_panel = TabbedPanel(do_default_tab=False, size_hint=(1,0.9), tab_width=200)
        key_order = [key for key in measure_dict.keys()]
        key_order.sort()

        #Creatings the buttons :
        self.liste_AI = 8
        self.liste_DI = 8
        self.liste_AO = 2
        self.liste_DO = 7
        self.liste_Address = 8

        self.used_ai, self.used_di, self.used_ao, self.used_do = self.used_pin(measure_dict, fan)

        #Panel used to display the DAQ Pinout img
        self.daq_pan = TabbedPanelItem(text="DAQ")
        self.tab_panel.add_widget(self.daq_pan)
        self.daq_pan_layout = BoxLayout(orientation = "vertical")
        self.daq_pan.add_widget(self.daq_pan_layout)


        self.daq_image= Image(source=DAC_IMAGE, size_hint=(1,0.9))
        self.daq_address = PortButton(self.liste_Address, "ADD", "ADD", fan.dac_address)
        self.daq_address.size_hint = (1,0.1)
        self.daq_pan_layout.add_widget(self.daq_image)
        self.daq_pan_layout.add_widget(self.daq_address)

        #Panel used to set the measures pinout
        self.measures_pan = TabbedPanelItem(text="Measures")
        self.tab_panel.add_widget(self.measures_pan)
        self.measures_pan_layout = BoxLayout(orientation = "vertical")
        self.measures_pan.add_widget(self.measures_pan_layout)
        #ordering the measure
        key_order = [key for key in measure_dict.keys()]
        key_order.sort()

        self.dict_measure_layout = {}
        self.dict_measure_toggle_button = {}
        self.dict_measure_label = {}
        for key in key_order :
            self.dict_measure_layout[key] = BoxLayout(orientation = "horizontal")
            self.dict_measure_label[key] = Label(text = str(key))
            self.dict_measure_layout[key].add_widget(self.dict_measure_label[key])
            self.dict_measure_toggle_button[key] = PortButton(self.liste_AI, "AI", key, measure_dict[key].port)
            self.dict_measure_layout[key].add_widget(self.dict_measure_toggle_button[key])
            self.measures_pan_layout.add_widget(self.dict_measure_layout[key])

        #Panel used to set the fan and transfo pinout
        self.fan_pan = TabbedPanelItem(text="Fan & Power & Flashlight")
        self.tab_panel.add_widget(self.fan_pan)
        self.fan_pan_layout = BoxLayout(orientation = "vertical")
        self.fan_pan.add_widget(self.fan_pan_layout)
        self.dict_fan_layout = {}
        self.dict_fan_toggle_button = {}

        key = 'flashLight'
        self.dict_fan_layout[key] = BoxLayout(orientation = "horizontal")
        self.dict_fan_layout[key].add_widget(Label(text = "FlashLight"))
        self.dict_fan_toggle_button[key] = PortButton(self.liste_DO, "DO", key, flash.port)
        self.dict_fan_layout[key].add_widget(self.dict_fan_toggle_button[key])
        self.fan_pan_layout.add_widget(self.dict_fan_layout[key])
        key = 'start'
        self.dict_fan_layout[key] = BoxLayout(orientation = "horizontal")
        self.dict_fan_layout[key].add_widget(Label(text = "Fan Start/Stop"))
        self.dict_fan_toggle_button[key] = PortButton(self.liste_DO, "DO", key, fan.start)
        self.dict_fan_layout[key].add_widget(self.dict_fan_toggle_button[key])
        self.fan_pan_layout.add_widget(self.dict_fan_layout[key])
        key = 'read'
        self.dict_fan_layout[key] = BoxLayout(orientation = "horizontal")
        self.dict_fan_layout[key].add_widget(Label(text = "Fan Status"))
        self.dict_fan_toggle_button[key] = PortButton(self.liste_DI, "DI", key, fan.read)
        self.dict_fan_layout[key].add_widget(self.dict_fan_toggle_button[key])
        self.fan_pan_layout.add_widget(self.dict_fan_layout[key])
        key = 'write'
        self.dict_fan_layout[key] = BoxLayout(orientation = "horizontal")
        self.dict_fan_layout[key].add_widget(Label(text = "Power Command Output"))
        self.dict_fan_toggle_button[key] = PortButton(self.liste_AO, "AO", key, fan.write)
        self.dict_fan_layout[key].add_widget(self.dict_fan_toggle_button[key])
        self.fan_pan_layout.add_widget(self.dict_fan_layout[key])
        key = 'redled'
        self.dict_fan_layout[key] = BoxLayout(orientation = "horizontal")
        self.dict_fan_layout[key].add_widget(Label(text = "Red Led"))
        self.dict_fan_toggle_button[key] = PortButton(self.liste_DO, "DO", key, fan.redled)
        self.dict_fan_layout[key].add_widget(self.dict_fan_toggle_button[key])
        self.fan_pan_layout.add_widget(self.dict_fan_layout[key])
        key = 'greenled'
        self.dict_fan_layout[key] = BoxLayout(orientation = "horizontal")
        self.dict_fan_layout[key].add_widget(Label(text = "Green Led"))
        self.dict_fan_toggle_button[key] = PortButton(self.liste_DO, "DO", key, fan.greenled)
        self.dict_fan_layout[key].add_widget(self.dict_fan_toggle_button[key])
        self.fan_pan_layout.add_widget(self.dict_fan_layout[key])
        key = 'relay'
        self.dict_fan_layout[key] = BoxLayout(orientation = "horizontal")
        self.dict_fan_layout[key].add_widget(Label(text = "Power Relay Command"))
        self.dict_fan_toggle_button[key] = PortButton(self.liste_DO, "DO", key, fan.relay)
        self.dict_fan_layout[key].add_widget(self.dict_fan_toggle_button[key])
        self.fan_pan_layout.add_widget(self.dict_fan_layout[key])
        key = 'switch'
        self.dict_fan_layout[key] = BoxLayout(orientation = "horizontal")
        self.dict_fan_layout[key].add_widget(Label(text = "Hard Stop Switch Status"))
        self.dict_fan_toggle_button[key] = PortButton(self.liste_DI, "DI", key, fan.switch)
        self.dict_fan_layout[key].add_widget(self.dict_fan_toggle_button[key])
        self.fan_pan_layout.add_widget(self.dict_fan_layout[key])


        self.button_layout = BoxLayout(orientation='horizontal', size_hint=(1, 0.1))
        self.save_button = Button(text="Save", on_press=lambda a:self.save_pressed())
        self.button_layout.add_widget(self.save_button)
        self.reset_button = Button(text="Reset", on_press=lambda a:self.reload_pressed())
        self.button_layout.add_widget(self.reset_button)


        self.main_layout.add_widget(self.tab_panel)
        self.main_layout.add_widget(self.button_layout)
        self.add_widget(self.main_layout)

    def used_pin(self, measure_dict, fan):
        """ Method used to create lists of all the current used pin.
        It's allow the software to check double assignations"""
        used_ai = []
        used_di = []
        used_ao = []
        used_do = []
        for key in measure_dict.keys():
            used_ai.append(measure_dict[key].port)

        used_di.append(fan.read)
        used_ao.append(fan.write)
        used_do.append(fan.redled)
        used_do.append(fan.greenled)
        used_do.append(fan.relay)
        used_di.append(fan.switch)

        return used_ai, used_di, used_ao, used_do

    def update_key(self, key_bind):
        """Method called when the name of a measure is change (ie. in the ParametersScreen)
        It then update the value of the keys (which are the label themselves) in the
        dictionary"""
        for key in self.dict_measure_layout.keys():
            val2 = self.dict_measure_layout.pop(key)
            self.dict_measure_layout[key_bind[key]] = val2
            val3 = self.dict_measure_toggle_button.pop(key)
            self.dict_measure_toggle_button[key_bind[key]] = val3
            self.dict_measure_toggle_button[key_bind[key]].update_group(key_bind[key])
            val4 = self.dict_measure_label.pop(key)
            self.dict_measure_label[key_bind[key]] = val4
            self.dict_measure_label[key_bind[key]].text = key_bind[key]

    def on_pre_enter(self):
        """ The on_pre_enter method is a binded method of the kivy environment.
        It's called when the screen is displayed, just before it appears.
        It's used to clean all the unsaved parameters"""
        self.reload_pressed()

    def reload_pressed(self):
        """ Reset the parameters to the last saved"""
        for key, value in measuresScreen.dict_measures.items() :
            self.dict_measure_toggle_button[key].set_used(value.port)
        self.dict_fan_toggle_button["flashLight"].set_used(measuresScreen.flash.port)
        self.dict_fan_toggle_button["start"].set_used(measuresScreen.fan_layout.start)
        self.dict_fan_toggle_button["read"].set_used(measuresScreen.fan_layout.read)
        self.dict_fan_toggle_button["write"].set_used(measuresScreen.fan_layout.write)
        self.dict_fan_toggle_button["redled"].set_used(measuresScreen.fan_layout.redled)
        self.dict_fan_toggle_button["greenled"].set_used(measuresScreen.fan_layout.greenled)
        self.dict_fan_toggle_button["relay"].set_used(measuresScreen.fan_layout.relay)
        self.dict_fan_toggle_button["switch"].set_used(measuresScreen.fan_layout.switch)

    def save_pressed(self):
        """ Update all the objects to the new values"""
        liste_used = []
        liste_double = []
        dict_measures_port = {}
        #Get DAQ Address :
        DAQ_ADDRESS = self.daq_address.return_number()
        #Check unused
        for key, button in self.dict_measure_toggle_button.items():
            value = button.return_used()
            dict_measures_port[key] = button.return_number()
            if value not in liste_used :
                liste_used.append(value)
            else :
                liste_double.append('Port "{}" is already used ({})'.format(value, key))

        for key, button in self.dict_fan_toggle_button.items():
            value = button.return_used()
            if value not in liste_used :
                liste_used.append(value)
            else :
                liste_double.append('Port "{}" is already used ({})'.format(value, key))

        #liste double countain the list of port already used...
        #Show a popup and abort saving action
        if liste_double :
            txt = ""
            for err in liste_double :
                txt += err + "\n"
            Popup(title='There are some errors...',
            content=Label(text=txt),size_hint=(None, None), size=(400, 400)).open()
            return False

        #update the fan conf
        measuresScreen.fan_layout.dac_address = DAQ_ADDRESS
        measuresScreen.fan_layout.start = self.dict_fan_toggle_button["start"].return_number()
        measuresScreen.fan_layout.read = self.dict_fan_toggle_button["read"].return_number()
        measuresScreen.fan_layout.write = self.dict_fan_toggle_button["write"].return_number()
        measuresScreen.fan_layout.redled = self.dict_fan_toggle_button["redled"].return_number()
        measuresScreen.fan_layout.greenled = self.dict_fan_toggle_button["greenled"].return_number()
        measuresScreen.fan_layout.relay = self.dict_fan_toggle_button["relay"].return_number()
        measuresScreen.fan_layout.switch = self.dict_fan_toggle_button["switch"].return_number()

        #update the measure conf
        measuresScreen.flash.port = self.dict_fan_toggle_button["flashLight"].return_number()

        #update the measures conf
        measuresScreen.update_pinout(dict_measures_port, DAQ_ADDRESS)
        measuresScreen.saveConf()
        Popup(title='Configuration Update',
        content=Label(text="The configuration has been saved"),
        size_hint=(None, None), size=(400, 400)).open()

class MainContener(BoxLayout):
    """ Used to inheritage. Create a MainContener Class based on the BoxLayout"""
    pass

class ConfigError(Exception):
    """ Error inheritage classe used when an misconfiguration is founded in the
    conf file"""
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        if self.message:
            return '{0} '.format(self.message)
        else:
            return 'Config Error'

class Configuration:
    """ Configuarion Class is used to read the configuration file.
    It uses the sp.py module to parse the txt file.
    Configuration contain all the information about measures"""
    def __init__(self):
        self.file = HOME_CONF_FILE
        self.dict_measure, self.fan, self.flash = self.__confCheck()

    def __confParser(self):
        """ Main parser rules. It describes the mandatory layout of the txt file"""
        label       = sp.R(r'[a-zA-Z]\w*')
        intv        = sp.R(r'\d*') /int
        floatv      = sp.R(r'([-+]?([0-9]*\.[0-9]+|[0-9]+))')
        blancs      = sp.R(r'\s+')
        commentaire = sp.R(r'#.*')

        with sp.Separator( blancs | commentaire):
            conf           = sp.Rule()
            debut          = sp.Rule()
            dacAddresse    = sp.Rule()
            measure        = sp.Rule()
            fan            = sp.Rule()
            titre          = sp.Rule()
            param          = sp.Rule()
            param_name     = sp.Rule()
            value          = sp.Rule()
            fin            = sp.Rule()

            conf         |= debut & dacAddresse & measure[:] & fan & fin
            debut        |= '<CONF>'
            dacAddresse  |= '<MISC>' & param[:]  & "</MISC>"
            measure      |= '<MEASURE LABEL="'& titre & '"' & '>' & param[:] & '</MEASURE>'
            fan          |= '<FAN>' & param[:] & '</FAN>'
            titre        |= label
            param        |= param_name & ":" & value & ";"
            param_name   |= label
            value        |= floatv
            fin          |= '</CONF>'

        return conf

    def __confAnalyse(self):
        """ Method use to read the txt file and apply the parser (__confParser)
        This method raise an error if a misconfiguration is triggered"""
        decoder=self.__confParser()
        try :
            with open(self.file, 'r') as f :
                liste_depart=decoder(f.read())
        except SyntaxError as erreur :
            print("-Erreur detectée dans le fichier de Config à [ligne,colonne]:")
            print('  ',erreur)
            liste_depart =[]
            exit()
        return liste_depart

    def __confCheck(self):
        """ Method used to check the result of the parser.
        If the parser doesn't raise an error, it only means that the layout of
        the file is good. The __confCheck method check the content. It's used in
        order to check that the content is correct. For example, a pin can only
        be used once..."""
        liste_conf = self.__confAnalyse()

        dict_measure = {}
        ### CHECK DAC_ADDRESS

        liste_misc = ["DAQ", "FLASH", "FLASHTRIGGER", "FLASHPOWERCOMMAND", "FLASHFAN"]
        liste_measure = ["PORT", "ROW", "COL", "RATIO", "MIN", "MAX", "TICK"]
        liste_fan = ["START", "READ", "WRITE", "RISETIME", "REDLED", "GREENLED", "RELAY", "SWITCH"]

        #Param used to check unused port :
        liste_AI = []
        liste_DI = []
        liste_AO = []
        liste_DO = []

        #Table correspondance :
        dict_keyword_port = {"FLASH": (liste_DO, "DO"),
                             "START": (liste_DO, "DO"),
                             "PORT": (liste_AI, "AI"),
                             "READ" : (liste_DI, "DI"),
                             "WRITE" : (liste_AO, "AO"),
                             "REDLED" : (liste_DO, "DO"),
                             "GREENLED" : (liste_DO, "DO"),
                             "RELAY" : (liste_DO, "DO"),
                             "SWITCH" : (liste_DI, "DI"),}

        ### CHECK MISC LINES
        dict_misc = {}
        misc_lines = liste_conf[0]
        l_params = []
        for params in misc_lines :
            param_name = params[0]
            param_value = int(float(params[1][0]))
            l_params.append(param_name)
            dict_misc[param_name]= param_value
            if param_name in dict_keyword_port.keys() :
                param_value = int(param_value)
                #Check that port is not used
                if param_value in dict_keyword_port[param_name][0] :
                    raise ConfigError("Already used port {}{}".format(dict_keyword_port[param_name][1], param_value))
                else :
                    dict_keyword_port[param_name][0].append(param_value)
        for aParam in liste_misc :
            if aParam not in l_params :
                raise ConfigError("Missing '{}' parameter in Fan configuration".format(aParam))
        dac_address = int(dict_misc["DAQ"])
        flash = FlashLight(dac_address, dict_misc["FLASH"], dict_misc["FLASHTRIGGER"], dict_misc["FLASHPOWERCOMMAND"], dict_misc["FLASHFAN"])

        ### CHECK MEASURE LINES
        measure_lines = liste_conf[1]

        liste_name = []
        liste_row_col = []
        for measure in measure_lines :
            measure_name = measure[0].replace('_',' ').replace('-', '')
            if measure_name in liste_name :
                raise ConfigError("Duplicated '{}' Measure Label".format(measure_name))
            liste_name.append(measure_name)
            conf_params = measure[1]
            l_params = []
            dict_params = {}
            dict_params['DAC_ADDRESS'] = dac_address
            dict_params['LABEL'] = measure_name
            for params in conf_params :
                param_name = params[0]
                param_value = float(params[1][0])
                l_params.append(param_name)
                dict_params[param_name]= param_value
                if param_name == "PORT" :
                    param_value = int(param_value)
                    #Check that port is not used
                    if param_value in dict_keyword_port[param_name][0] :
                        raise ConfigError("Already used port {}{}".format(dict_keyword_port[param_name][1], param_value))
                    else :
                        dict_keyword_port[param_name][0].append(param_value)

            for aParam in liste_measure :
                if aParam not in l_params :
                    raise ConfigError("Missing '{}' parameter in Measure '{}' configuration".format(aParam, measure_name))

            row_col = (int(dict_params['ROW']), int(dict_params['COL']))
            if row_col in liste_row_col :
                raise ConfigError("Duplicated Position @{}".format(row_col))
            liste_row_col.append(row_col)
            dict_measure[measure_name] = VuMeter(dict_params)

        ### CHECK FAN LINE
        dict_fan = {}
        dict_fan['DAC_ADDRESS'] = dac_address
        fan_line = liste_conf[2]
        l_params = []
        for params in fan_line :
            param_name = params[0]
            param_value = float(params[1][0])
            l_params.append(param_name)
            dict_fan[param_name]= param_value
            if param_name in dict_keyword_port.keys() :
                param_value = int(param_value)
                #Check that port is not used
                if param_value in dict_keyword_port[param_name][0] :
                    raise ConfigError("Already used port {}{}".format(dict_keyword_port[param_name][1], param_value))
                else :
                    dict_keyword_port[param_name][0].append(param_value)

        for aParam in liste_fan :
            if aParam not in l_params :
                raise ConfigError("Missing '{}' parameter in Fan configuration".format(aParam))
        fan = FanMeter(dict_fan)
        return dict_measure, fan, flash

class PILCApp(App):
    """ Main app class"""
    title = 'PI-LC'
    icon = CERN_LOGO

    def __init__(self, **kwargs):
        super(PILCApp, self).__init__(**kwargs)

        self.configuration = Configuration()
        #The float parameters correspond to the refresh rate in seconds
        # 0.01 means 100 Hertz, 100 frame par seconds.
        Clock.schedule_interval(self.update_clock, 0.01)

    def build(self):
        """ The build method is a mandatory method by the kivy environment. It's
        called when the main windows is builded"""

        self.load_kv(KV_FILE)
        self.container = MainContener()

        #Creating the three main screen and the global var assiociated
        self.sm = ScreenManager(transition = CardTransition())
        global measuresScreen
        measuresScreen = MeasuresScreen(self.configuration.dict_measure,
                                       self.configuration.fan, self.configuration.flash)
        self.measures = measuresScreen

        global parametersScreen
        parametersScreen = ParametersScreen(self.configuration.dict_measure,
                                       self.configuration.fan, self.configuration.flash)
        self.parameters = parametersScreen

        global pinoutScreen
        pinoutScreen = PinoutScreen(self.configuration.dict_measure,
                                       self.configuration.fan, self.configuration.flash)

        self.pinout = pinoutScreen
        self.dict_screens = {'measures': measuresScreen,
                             'parameters': parametersScreen,
                             'pinout' : pinoutScreen}


        self.sm.add_widget(measuresScreen)
        self.sm.add_widget(parametersScreen)
        self.sm.add_widget(pinoutScreen)

        self.container.add_widget(self.sm)

        return self.container

    def go_to_screen(self, idx):
        """ Method used by the action bar to change the current screen"""
        self.sm.switch_to(self.dict_screens[idx])

    def update_clock(self, *largs):
        """Method called for each frame refresh"""
        #FanState is a Tuple of Bool with first representing the fan control
        #and second the power command
        fan_state = self.configuration.fan.toogleFan()
        liste_val = [val.update_val() for key, val in self.measures.dict_measures.items()]
        self.configuration.flash.toggleFlash(fan_state, liste_val)

def allOff():
    """All off fonction set all the output of all the DACQPlate to 0"""
    DAQlist = DAQ.daqcsPresent
    ad = 0
    for i in DAQ.daqcsPresent:
        if i == 1 :
            DAQ.setDOUTall(ad,0)
            DAQ.setDAC(ad,0,0)
            DAQ.setDAC(ad,1,0)
        ad+=1

def handle_exit(sig, frame):
    """ Handle a System Exit (System Shutdown)
    It's used to turn off all the output when the Pi is turned off"""
    allOff()
    raise(SystemExit)

def runPiLC():
    """ Main function. Used to launch the main window. Turn all the output to
    0 when exiting the app"""
    #Catching the sigterm
    signal.signal(signal.SIGTERM, handle_exit)
    Config.set('graphics', 'width', '800')
    Config.set('graphics', 'height', '480')
    Config.set('kivy', 'keyboard_mode', 'systemandmulti')
    Config.set('graphics', 'window_state', 'maximized')
    try :
        PILCApp().run()
    finally:
        allOff()


if __name__ == '__main__':
    runPiLC()
