![pilcbanner](https://gitlab.cern.ch/be-rf-is/pi-lc/-/raw/a97811288908930e1c3d562459a679daf36787a1/DocLatex/Imgs/PILC_banner.jpg)
# Pi-LC
A small PLC-style project made with a Raspberry
This project has been created in order to protect the 10 MHz Feedback Amplifiers from the CERN PS RF System during their tests.

The PI-LC :

 - Monitor bias voltages
 - Monitor the fan status and trigger the heating power
 - Allows parameters change
 - Allows dynamic pinout modifications

## Installation
 1. Login via SSH or via the console.
 2. Run the following command and PiLC will be setup, installed and added to startup:

```bash
curl -sSL http://cern.ch/go/L6tg | sudo bash
```
### What does install script do ?
 - [x] Install Python3 if needed
 - [x] Install Pi-LC software and all its dependencies
 - [x] Create the /opt/PiLC folder wich contain the start and stop scripts used after boot and before shutdown
 - [x] Add a pilc.desktop file to /home/pi/.config/autostart to autostart the software when user pi login
 - [x] Add and enable the stop_pilc service to turn off all output when the Raspbery shutdown

### What does install script does not do (Means that it shall be down by yourself) ?
 - [ ] Enabling the SPI in the Raspberry configuration (use `sudo raspi-config` to perform)
 - [ ] Enabling the One-Wire in the Raspberry configuration (use `sudo raspi-config` to perform)
 - [ ] Auto-reducing the task bar to use the whole scring : right click the task bar, select parameters then advanced and tick the "auto reduce..."


## Raspberry 7" TouchScreen
If you are using the official 7" Raspberry touch screen, you'll have to permform the following operation in order use it :
```bash
nano ~/.kivy/config.ini
```
Then go to the [input] section and add this :

    mouse = mouse
    mtdev_%(name)s = probesysfs,provider=mtdev
    hid_%(name)s = probesysfs,provider=hidinput

Save the file and reboot, you'll now be able to use the touchscreen as an input.

## Usage
The Pi-LC software is started fullwindow just after booting. If not existing the configuration file is stored in :

    > /home/{user}/.pilc/conf.txt

Main windows is divided in three part :

 ###  ![measure](https://gitlab.cern.ch/be-rf-is/pi-lc/-/raw/a97811288908930e1c3d562459a679daf36787a1/pilc/data/icons/measures.png) Measures

This is the main tab. Here are displayed the current values of the mesured pin. It also display the fan status and the power command.

 ###  ![parameters](https://gitlab.cern.ch/be-rf-is/pi-lc/-/raw/a97811288908930e1c3d562459a679daf36787a1/pilc/data/icons/cogs.png)Parameters

This tab display a form for each measure. Each form is made to control the display of the vu-meters, and some paremeters about the measure.

###  ![pinout](https://gitlab.cern.ch/be-rf-is/pi-lc/-/raw/a97811288908930e1c3d562459a679daf36787a1/pilc/data/icons/wiring.png)Pinout

This tab is made to allows the user to dynamically change the pin where the software make his measures.

**For the Parameters and Pinout tab, change are not taken in account if the user don't click on "save"**

### Adding a new measure
It's not possible to add a new measure within the GUI. If you want to perform this kind of action, you'll have to edit the configuration file :

> nano /home/{user}/.pilc/conf.txt

And add a new `<MEASURE>` section in the file. Here is the template for a measure :

    <MEASURE LABEL="UG1_Driver">
            PORT : 0;
            ROW : 1;
            COL : 0;
            RATIO : -17.08;
            MIN : -15;
            MAX : -10;
            TICK : 10;
    </MEASURE>
