#!/usr/bin/env bash

#Check if script is being run as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

if [ ! $? = 0 ]; then
   exit 1
else
  apt-get update
  apt-get install -y git whiptail


  PiLCDir="pi-lc"
  git clone https://gitlab.cern.ch/be-rf-is/pi-lc.git

  apt-get install -y python3 python3-pip

  apt-get install -y libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev

  apt-get install -y pkg-config libgl1-mesa-dev libgles2-mesa-dev

  apt-get install -y python-setuptools libgstreamer1.0-dev git-core

  apt-get install -y gstreamer1.0-plugins-{bad,base,good,ugly}

  apt-get install -y gstreamer1.0-{omx,alsa} python-dev libmtdev-dev

  apt-get install -y xclip xsel libjpeg-dev

  python3 -m pip install --upgrade pip setuptools

  python3 -m pip install --upgrade Cython==0.29.10 pillow

  python3 -m pip install kivy

  python3 -m pip install https://github.com/kivy-garden/speedmeter/archive/master.zip

  cd $PiLCDir
  python3 setup.py install

  cd ..
  if [ ! -d "/opt/PiLC" ]
  then
     mkdir /opt/PiLC
  fi
  cp $PiLCDir/install_files/pilc_start.py /opt/PiLC
  cp $PiLCDir/install_files/allOff.py /opt/PiLC
  chmod +x /opt/PiLC/allOff.py
  chmod +x /opt/PiLC/pilc_start.py
  if [ ! -d "/home/pi/.config/autostart/" ]
  then
     mkdir /home/pi/.config/autostart/
  fi
  cp $PiLCDir/install_files/pilc.desktop /home/pi/.config/autostart/
  cp $PiLCDir/install_files/stop_pilc.service /etc/systemd/system
  systemctl enable /etc/systemd/system/stop_pilc.service
  rm -rf $PiLCDir
  whiptail --title "Installation complete" --msgbox "PiLC installation complete." 8 78
fi
