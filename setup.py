#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
import pilc


setup(
    name='pilc',
    version=pilc.__version__,
    packages=find_packages(),
    author="Cédric LOMBARD",
    author_email="cedric.lombard@cern.ch",
    description="A PLC style project made with Kivy",
    long_description=open('README.md').read(),
    include_package_data=True,
    url='https://gitlab.cern.ch/be-rf-is/pi-lc/',
    classifiers=[
        "Programming Language :: Python",
        "Development Status :: 5 - Production/Stable",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3.7",
        "Topic :: Scientific/Engineering :: Visualization",
    ],
    entry_points = {
        'console_scripts': [
            'pilc = pilc.PILC:runPiLC',
        ],
    },
    license="MIT",
    install_requires=[
   "certifi>=2020.4.5.1",
   "chardet>=3.0.4",
   "docutils>=0.16",
   "idna>=2.9",
   "pi-plates>=6.2",
   "Pygments>=2.6.1",
   "requests>=2.23.0",
   "RPi.GPIO>=0.7.0",
   "six>=1.14.0",
   "spi>=0.2.0",
   "spidev>=3.4",
   "urllib3>=1.25.9",],
)
