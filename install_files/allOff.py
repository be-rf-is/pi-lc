#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import piplates.DAQCplate as DAQ
DAQlist = DAQ.daqcsPresent
ad = 0
for i in DAQ.daqcsPresent:
    if i == 1 :
        DAQ.setDOUTall(ad,0)
        DAQ.setDAC(ad,0,0)
        DAQ.setDAC(ad,1,0)
    ad+=1
